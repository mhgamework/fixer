﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MHGameWork.Fixer
{
    public class Configuration
    {
        private static Configuration _current;
        public static Configuration Current
        {
            get
            {
                if (_current == null)
                    Load();
                return _current;
            }
        }

        private static Configuration loadConfiguration()
        {
            if (!File.Exists(Files.ConfigurationFile))
                return CreateDefault();

            var ser = new XmlSerializer(typeof(Configuration));
            using (var fs = File.OpenRead(Files.ConfigurationFile))
                return (Configuration)ser.Deserialize(fs);

        }
        private static void saveConfiguration(Configuration c)
        {
            var ser = new XmlSerializer(typeof(Configuration));
            using (var fs = File.OpenWrite(Files.ConfigurationFile))
                ser.Serialize(fs, c);
        }

        public static Configuration CreateDefault()
        {
            return new Configuration();
        }



        public static void Load()
        {
            _current = loadConfiguration();
        }
        public static void Save()
        {
            if (_current == null) throw new InvalidOperationException("Configuration not loaded yet!");
            saveConfiguration(_current);
        }





        /// <summary>
        /// True when fixer should attempt an online update
        /// </summary>
        public bool UpdatingEnabled = true;

        /// <summary>
        /// When this is set to true, fixer will run at the current started location.
        /// When set to false, fixer will exit and start the executable at the installation location (program files)
        /// </summary>
        public bool RunLocal = false;

        /// <summary>
        /// The root path of the Fixer package depository web service
        /// </summary>
        public string WebServiceRoot = "http://fixer.thewizards.be/";
    }
}
