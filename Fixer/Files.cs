﻿using System;
using System.IO;

namespace MHGameWork.Fixer
{
    public static class Files
    {
        private static string coreDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                                "\\MHGameWork\\Fixer\\Binaries";


        public static string CoreDirectory
        {
            get
            {
                return coreDirectory;
            }
            set { coreDirectory = value; }
        }

        public static string FixerExecutable { get { return CoreDirectory + "\\Fixer.exe"; } }

        public static string LastRunFile { get { return CoreDirectory + "\\lastRun"; } }
        public static string ConfigurationFile { get { return Environment.CurrentDirectory + "\\config.xml"; } }



        public static string UpdatePackFile { get { return CoreDirectory + "\\update.zip"; } }
        public static string UnpackerExecutable { get { return CoreDirectory + "\\Unpacker.exe"; } }

        public static void CreateAllDirectories()
        {
            Directory.CreateDirectory(CoreDirectory);
        }
    }
}
