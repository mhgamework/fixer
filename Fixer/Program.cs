﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows;

namespace MHGameWork.Fixer
{
    class Program
    {
        public static void Main(string[] args)
        {

            InstallAssemblyResolveHandler();
            Files.CreateAllDirectories();

            Configuration.Load();

            if (Configuration.Current.RunLocal)
            {
                Files.CoreDirectory = Environment.CurrentDirectory;
            }




            Thread.Sleep(2000); // Basic safety for process loop.

            var updater = new CoreUpdater();

            if (Configuration.Current.UpdatingEnabled && !updater.IsUpToDate())
            {
                updater.Update();
                return;
            }

            if (Assembly.GetExecutingAssembly().Location != Files.FixerExecutable)
            {
                // Not running main executable!, run the main executable!!
                Process.Start(Files.FixerExecutable);
                return;
            }


            Configuration.Save();
            // Start normally
            MessageBox.Show("HI");
        }



        private static void InstallAssemblyResolveHandler()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
                {
                    String resourceName = "AssemblyLoadingAndReflection." +
                                            new AssemblyName(args.Name).Name + ".dll";
                    using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
                    {

                        Byte[] assemblyData = new Byte[stream.Length];
                        stream.Read(assemblyData, 0, assemblyData.Length);
                        return Assembly.Load(assemblyData);
                    }
                };
        }
    }
}
