﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Linq;
using System.Text;

namespace MHGameWork.Fixer
{
    public class CoreUpdater
    {
        public static string GetUpdatePackURL = Configuration.Current.WebServiceRoot +  "/Update/UpdatePack.zip";
        public static string GetUpdateMD5 = Configuration.Current.WebServiceRoot +  "/Update/UpdateMD5.php";

        public bool IsUpToDate()
        {

            if (!File.Exists(Files.FixerExecutable))
                return false;

            using (HashAlgorithm hashAlg = MD5.Create())
            {
                using (Stream file = new FileStream(Files.UpdatePackFile, FileMode.Open, FileAccess.Read))
                {
                    byte[] hash = hashAlg.ComputeHash(file);


                    // step 2, convert byte array to hex string
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < hash.Length; i++)
                    {
                        sb.Append(hash[i].ToString("X2"));
                    }
                    var hashStr = sb.ToString();

                    var onlineHash = getNewestExecutableHexHash();

                    if (hashStr.ToLower() != onlineHash.ToLower())
                        return false;

                }
            }
            
            return true;
        }


        private string getNewestExecutableHexHash()
        {
            var req = WebRequest.Create(GetUpdateMD5);

            var response = req.GetResponse();
            if (response == null)
                Environment.FailFast("Unable to get executable hash from the server!");

            var strm = response.GetResponseStream();

            if (strm == null)
                throw new InvalidOperationException("Remote update server not accessible!");

            var stream = response.GetResponseStream();
            var reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }




        public void Update()
        {
            DownloadUpdatePack(Files.UpdatePackFile);

            CreateUpdateToolExecutable();
            RunUpdateTool();
        }


        private void RunUpdateTool()
        {
            var info = new ProcessStartInfo(Files.UnpackerExecutable, String.Format("\"{0}\" \"{1}\" \"{2}\"",
                                            Files.UpdatePackFile, Files.CoreDirectory, Files.FixerExecutable));
            Process.Start(info);
        }

        private void CreateUpdateToolExecutable()
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("MHGameWork.Fixer.Unpacker.exe"))
            {
                if (stream == null) throw new InvalidOperationException();
                using (var fs = File.Create(Files.UnpackerExecutable))
                    stream.CopyTo(fs);
            }

        }

        private void DownloadUpdatePack(string targetLocation)
        {
            var req = WebRequest.Create(GetUpdatePackURL);

            var response = req.GetResponse();
            if (response == null)
                Environment.FailFast("Unable to get update pack from the server!");

            var strm = response.GetResponseStream();

            using (var fs = File.Create(targetLocation))
                strm.CopyTo(fs, 128 * 1024);
        }
    }
}
