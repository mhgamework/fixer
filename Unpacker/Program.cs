﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;

namespace Unpacker
{
    class Program
    {
        static void Main(string[] args)
        {
            InstallAssemblyResolveHandler();

            if (args[0] == "--test")
            {
                test();
                return;
            }
            Unpack(args[0], args[1]);

            Process.Start(args[2]);
        }

        private static void test()
        {
            Pack("test\\test.zip", "test\\data");
            Unpack("test\\test.zip", "test\\Unpacked");
        }

        public static void Unpack(string zipFile, string targetDirectory)
        {
            var zip = new FastZip();
            zip.ExtractZip(zipFile, targetDirectory, "");
        }
        public static void Pack(string zipFile, string sourceDirectory)
        {
            var zip = new FastZip();
            zip.CreateZip(zipFile, sourceDirectory, true, "");
        }

        private static void InstallAssemblyResolveHandler()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
            {
                String resourceName = typeof(Program).Namespace + "." +
                                        new AssemblyName(args.Name).Name + ".dll";
                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
                {

                    Byte[] assemblyData = new Byte[stream.Length];
                    stream.Read(assemblyData, 0, assemblyData.Length);
                    return Assembly.Load(assemblyData);
                }
            };
        }
    }
}
